#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/JointState.h>
#include <sensor_msgs/Range.h>
#include <geometry_msgs/Twist.h>
#include <tf/tf.h>
#include "orazio_client.h"
#include <iostream>
using namespace std;

const double uint16_to_radians = 2*M_PI/65536;
#define NUM_JOINTS_MAX 4

DifferentialDriveControlPacket drive_control = {
  {
    .type=DIFFERENTIAL_DRIVE_CONTROL_PACKET_ID,
    .size=sizeof(DifferentialDriveControlPacket),
    .seq=0
  },
  .translational_velocity=0,
  .rotational_velocity=0
};

SystemStatusPacket system_status={
  .header={
    .type=SYSTEM_STATUS_PACKET_ID,
    .size=sizeof(SystemStatusPacket)
  }
};

SystemParamPacket system_params={
  .header={
    .type=SYSTEM_PARAM_PACKET_ID,
    .size=sizeof(SystemParamPacket)
  }
};


SonarStatusPacket sonar_status={
  .header={
    .type=SONAR_STATUS_PACKET_ID,
    .size=sizeof(SonarStatusPacket)
  }
};

SonarParamPacket sonar_params={
  .header={
    .type=SONAR_PARAM_PACKET_ID,
    .size=sizeof(SonarParamPacket)
  }
};

StringMessagePacket message={
  .header={
    .type=MESSAGE_PACKET_ID
  }
};

DifferentialDriveStatusPacket drive_status = {
  .header={
    .type=DIFFERENTIAL_DRIVE_STATUS_PACKET_ID,
    .size=sizeof(DifferentialDriveStatusPacket)
  }
};
  
DifferentialDriveParamPacket drive_params = {
  .header={
    .type=DIFFERENTIAL_DRIVE_PARAM_PACKET_ID,
    .size=sizeof(DifferentialDriveParamPacket)
  }
};

EndEpochPacket end_epoch = {
  .type=END_EPOCH_PACKET_ID,
  .size=sizeof(EndEpochPacket)
};

ResponsePacket response = {
  .header={
    .type=RESPONSE_PACKET_ID,
    .size=sizeof(ResponsePacket)
  }
};

ParamControlPacket param_control={
  .header={
    .type=PARAM_CONTROL_PACKET_ID,
    .size=sizeof(ParamControlPacket),
    .seq=0
  },
  .action=ParamRequest,
  .param_type=ParamSystem
};

JointStatusPacket joint_status[NUM_JOINTS_MAX];


// each time we receive a packet we mark to 1 the seq to send it
void commandVelCallback(const geometry_msgs::TwistConstPtr twist){
  drive_control.translational_velocity=twist->linear.x;
  drive_control.rotational_velocity=twist->angular.z;
  drive_control.header.seq=1;
}

int main(int argc, char** argv) {
  std::string serial_device;
  std::string odom_topic;
  std::string sonar_topic;
  std::string ticks_topic;
  std::string joint_state_topic;
  std::string odom_frame_id;
  std::string command_vel_topic;
  std::string sonar_frame_id_prefix;

  for (int i=0; i<NUM_JOINTS_MAX; ++i){
    joint_status[i].header.header.type=JOINT_STATUS_PACKET_ID;
    joint_status[i].header.header.size=sizeof(JointStatusPacket);
    joint_status[i].header.index=i;
  }

  
  ros::init(argc, argv, "orazio_robot_node");
  ros::NodeHandle nh("~");
  nh.param("serial_device", serial_device, std::string("/dev/ttyACM0"));
  nh.param("odom_topic", odom_topic, std::string("/odom"));
  nh.param("ticks_topic", ticks_topic, std::string("/ticks"));
  nh.param("joint_state_topic", joint_state_topic, std::string("/joint_state"));
  nh.param("command_vel_topic", command_vel_topic, std::string("/cmd_vel"));
  nh.param("odom_frame_id", odom_frame_id, std::string("/odom"));
  nh.param("sonar_frame_id_prefix", sonar_frame_id_prefix, std::string("/sonar_frame"));
  nh.param("sonar_topic", sonar_topic, std::string("/sonar"));
  
  cerr << "running with params: ";
  cerr << "serial_device: " << serial_device << endl;
  cerr << "odom_topic: " << odom_topic << endl;
  cerr << "odom_frame_id: " << odom_frame_id << endl;
  cerr << "command_vel_topic: " << command_vel_topic << endl;
  cerr << "sonar_topic: " << sonar_topic << endl;
  cerr << "sonar_frame_id_prefix: " << sonar_frame_id_prefix << endl;
  
  ros::Subscriber command_vel_subscriber = nh.subscribe<geometry_msgs::TwistConstPtr>(command_vel_topic, 1, &commandVelCallback);
  ros::Publisher odom_publisher = nh.advertise<nav_msgs::Odometry>(odom_topic, 1);
  ros::Publisher joint_state_publisher = nh.advertise<sensor_msgs::JointState>(joint_state_topic, 1);


  sensor_msgs::Range sonar_msgs[SONARS_MAX];
  ros::Publisher sonar_publishers[SONARS_MAX];
  cerr << "System has up to " << SONARS_MAX << " sonars" << endl;
  for (int s=0; s<SONARS_MAX; ++s) {
    sensor_msgs::Range& sonar_msg=sonar_msgs[s];
    ros::Publisher& sonar_publisher=sonar_publishers[s];
    sonar_msg.radiation_type=sensor_msgs::Range::ULTRASOUND;
    sonar_msg.field_of_view=30.f/M_PI;
    sonar_msg.min_range=0.1f;
    sonar_msg.max_range=3.f;
    char name_buffer[1024];
    sprintf(name_buffer, "%s_%d", sonar_frame_id_prefix.c_str(), s);
    sonar_msg.header.frame_id=std::string(name_buffer);
    sprintf(name_buffer, "%s_%d", sonar_topic.c_str(), s);
    sonar_publisher= nh.advertise<sensor_msgs::Range>(std::string(name_buffer),8);
  }
  
  struct OrazioClient* client=OrazioClient_init(serial_device.c_str(), 115200);
  if (! client) {
    cerr << "cannot open client on device [" << serial_device << "]\nABORTING" << endl;
    return -1;
  }

  cerr << "Synching...";
  OrazioClient_sync(client,50);
  printf(" Done\n");
  if (OrazioClient_readConfiguration(client, 100)!=Success){
    cerr << "could not readd the configuration" << endl;
    cerr << "this client is compiled with firmware version ";
    fprintf(stderr,"%08x\n",ORAZIO_PROTOCOL_VERSION);
    cerr << "check that the firmware/client have the same protocol version number" << endl;
    return -1;
  }

  nav_msgs::Odometry odom;
  sensor_msgs::JointState joint_state;
  int num_joints=OrazioClient_numJoints(client);
  joint_state.name.resize(num_joints);
  joint_state.position.resize(num_joints);
  joint_state.velocity.resize(num_joints);
  joint_state.effort.resize(num_joints);

  if(num_joints==2){
    joint_state.name[0]="left_wheel";
    joint_state.name[1]="right_wheel";
    joint_state.effort[0]=0;
    joint_state.effort[1]=0;
  }
  
  OrazioClient_get(client, (PacketHeader*)&system_params);
  OrazioClient_get(client, (PacketHeader*)&drive_params);
  OrazioClient_get(client, (PacketHeader*)&sonar_params);
  int left_joint_index=drive_params.left_joint_index;
  int right_joint_index=drive_params.right_joint_index;


  float timer_period=1e-3*system_params.timer_period_ms;
  odom.header.frame_id = odom_frame_id;

  // at the beginning we disable all packets, to minimize the burden on the serial line

   
  int seq = 0;
  while(ros::ok()){
    ros::spinOnce();
    if(drive_control.header.seq) {
      int result = OrazioClient_sendPacket(client, (PacketHeader*)&drive_control, 0);
      if (result)
        cerr << "send error" << endl;
      drive_control.header.seq=0;
    }
    // we check the subscribers
    uint8_t requested_packets=0;
    if (odom_publisher.getNumSubscribers()>0){
      requested_packets|=PDriveStatusFlag;
    }
    if (joint_state_publisher.getNumSubscribers()>0){
      requested_packets|=PJointStatusFlag;
    }
    bool has_sonar=false;
    for (int s=0; s<SONARS_MAX; ++s) {
      if (sonar_publishers[s].getNumSubscribers()>0){
        has_sonar=true;
        break;
      }
    }
    if (has_sonar){
      requested_packets|=PSonarStatusFlag;
    }

    if (system_params.periodic_packet_mask!=requested_packets){
      cerr << "changing periodic packets " << (int)system_params.periodic_packet_mask;
      cerr << " ->" << (int) requested_packets << endl;
      system_params.periodic_packet_mask=requested_packets;
      OrazioClient_sendPacket(client, (PacketHeader*)&system_params, 1);
      OrazioClient_get(client, (PacketHeader*)&system_params);
    }
    OrazioClient_sync(client,1);
    ros::Time this_time=ros::Time::now();

    // this is always sent and tells us which epoch we are in
    OrazioClient_get(client, (PacketHeader*)&end_epoch);
    
    // we retrieve the info from the client
    //OrazioClient_get(client, (PacketHeader*)&system_status, SYSTEM_STATUS_PACKET_ID);
    if (system_params.periodic_packet_mask&PJointStatusFlag) {
      joint_state.header=odom.header;
      if (num_joints==2) { // differential drive case
        OrazioClient_get(client, (PacketHeader*)&joint_status[left_joint_index]);
        OrazioClient_get(client, (PacketHeader*)&joint_status[right_joint_index]);
        uint16_t left_encoder_position=joint_status[left_joint_index].info.encoder_position;
        uint16_t right_encoder_position=joint_status[right_joint_index].info.encoder_position;
        int16_t left_encoder_speed=joint_status[left_joint_index].info.encoder_speed;
        int16_t right_encoder_speed=joint_status[right_joint_index].info.encoder_speed;
        joint_state.position[0]=uint16_to_radians*left_encoder_position;
        joint_state.position[1]=uint16_to_radians*right_encoder_position;
        joint_state.velocity[0]=uint16_to_radians*left_encoder_speed/timer_period;
        joint_state.velocity[1]=uint16_to_radians*right_encoder_speed/timer_period;
      } else { // all other cases
        for (int i=0; i<num_joints; ++i){
          OrazioClient_get(client, (PacketHeader*)&joint_status[i]);
          joint_state.position[i]=uint16_to_radians*joint_status[i].info.encoder_position;
          joint_state.velocity[i]=uint16_to_radians*joint_status[i].info.encoder_speed/timer_period;
        }
      }
      joint_state_publisher.publish(joint_state);
    }
    if (system_params.periodic_packet_mask&PDriveStatusFlag) {
      OrazioClient_get(client, (PacketHeader*)&drive_status);
      // send the odometry
      odom.header.seq = seq;
      odom.header.stamp = this_time;
      odom.pose.pose.position.x = drive_status.odom_x;
      odom.pose.pose.position.y = drive_status.odom_y;
      odom.pose.pose.position.z = 0;
      odom.pose.pose.orientation=tf::createQuaternionMsgFromYaw(drive_status.odom_theta);
      odom_publisher.publish(odom);
    }
    if (system_params.periodic_packet_mask&PSonarStatusFlag) {
      OrazioClient_get(client, (PacketHeader*)&sonar_status);
      // if the sonar is fresh, we shoot out a message for each valid reading
      if (sonar_status.header.seq==end_epoch.seq){
        for (int s=0; s<SONARS_MAX; ++s) {
          uint16_t r=sonar_status.ranges[s];
          if (! r)
            continue;
          sensor_msgs::Range& sonar_msg=sonar_msgs[s];
          ros::Publisher& sonar_publisher=sonar_publishers[s];
          sonar_msg.header.stamp=this_time;
          sonar_msg.range=0.001*r;
          sonar_publisher.publish(sonar_msg);
        }
      }
    }
   }
  cerr << "Shutting down" << endl;
  OrazioClient_destroy(client);
}
